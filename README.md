# Codethink Map Circles
A simple web page which uses Open Layers to draw an Open Street Map with some
circles on it, centered from a point.

I followed the [Open Layers Introduction](https://openlayers.org/en/latest/doc/tutorials/bundle.html) and copied [Tissot Indicatrix](https://openlayers.org/en/latest/examples/tissot.html).

Init and start dev:

    npm init
    npm start

Package into dist/:

    npm run build
