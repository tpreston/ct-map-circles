import 'ol/ol.css';
import Feature from 'ol/Feature';
import Map from 'ol/Map';
import View from 'ol/View';
import {circular as circularPolygon} from 'ol/geom/Polygon';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import TileWMS from 'ol/source/TileWMS';
import VectorSource from 'ol/source/Vector';

import {fromLonLat} from 'ol/proj';
import OSM from 'ol/source/OSM';
import {Fill, Stroke, Style} from 'ol/style';

const HOME = {
	"name": "Codethink", "lonlat": [-2.2331529, 53.4813058],
};

// Distance in meters
// TODO Use the Coordinates so we can place an icon, then calculate the distance
const DISTANCES = [
	{"name": "Codethink, England", "distance": 10},
	{"name": "Dublin, Ireland", "distance": 267000},
	{"name": "Brussels, Belgium", "distance": 537000},
	{"name": "Berlin, Germany", "distance": 1049000},
	{"name": "Almaria, Spain", "distance": 1850310},
	{"name": "Moscow, Russia", "distance": 2542376},
	{"name": "New York, USA", "distance": 5369881},
];

var vectorLayer3857 = new VectorLayer({
	source: new VectorSource(),
	style: new Style({
		stroke: new Stroke({
			color: 'lightgreen',
			width: 3
		}),
		fill: new Fill({
			color: 'rgba(0, 0, 255, 0)'
		})
	}),
});

var map3857 = new Map({
	layers: [
		new TileLayer({source: new OSM()}),
		vectorLayer3857
	],
	target: 'map3857',
	view: new View({
		center: fromLonLat(HOME["lonlat"]),
		zoom: 4
	})
});

for (var i = 0; i < DISTANCES.length; i++) {
	// circularPolygon returns a circle for a EPSG:4326 projection
	var circle4326 = circularPolygon(
		HOME["lonlat"], DISTANCES[i]["distance"], 64);
	var circle3857 = circle4326.clone().transform('EPSG:4326', 'EPSG:3857');
	vectorLayer3857.getSource().addFeature(new Feature(circle3857));
}
